
import unittest
import requests
import multiprocessing as mp
import sys
import os

from py_socks5_proxy.socksProxy import ThreadingTCPServer, SocksHandler


def run_socks5():
    with ThreadingTCPServer(('127.0.0.1', 9011), SocksHandler) as server:
        server.serve_forever()


class TestSocks5Proxy(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.server = mp.Process(target=run_socks5, args=())
        cls.server.start()

    @classmethod
    def tearDownClass(cls) -> None:
        cls.server.terminate()

    def test_without_proxy(self):
        rsp = requests.get("http://www.baidu.com")
        self.assertTrue(rsp.status_code, 200)

    def test_proxy(self):
        rsp = requests.get("http://www.baidu.com", proxies=dict(
            http='socks5://127.0.0.1:9011'))
        self.assertTrue(rsp.status_code, 200)


if __name__ == '__main__':
    unittest.main()
