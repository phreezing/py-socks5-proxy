from socketserver import ThreadingMixIn, TCPServer, StreamRequestHandler


import logging
import struct
import socket
import select

logging.basicConfig(level=logging.INFO)
SOCKS_VERSION = 5


class ThreadingTCPServer(ThreadingMixIn, TCPServer):
    pass


class SocksHandler(StreamRequestHandler):

    def handle(self):
        logging.info('socks proxy server running.')

        self.auth()
        remote = self.connect()
        self.forward(self.request, remote)

    def auth(self):
        logging.info('starting authorization for %s' %
                     ':'.join(map(str, self.request.getpeername())))

        version, nmethods = struct.unpack("!BB", self.request.recv(2))
        assert version == SOCKS_VERSION and nmethods > 0
        self.request.recv(nmethods)  # consume methods.

        self.request.sendall(struct.pack("!BB", SOCKS_VERSION, 0))

    def connect(self) -> socket:
        logging.info('establishing connection for client.')

        header = self.request.recv(4)
        logging.info(header)
        ver, cmd, _, addr_type = struct.unpack("!BBBB", header)

        assert ver == SOCKS_VERSION and cmd == 1

        if addr_type == 1:  # IPv4
            address = socket.inet_ntoa(self.request.recv(4))
        elif addr_type == 3:  # Domain name
            domain_length = self.request.recv(1)[0]
            address = socket.gethostbyname(self.request.recv(domain_length))
        else:
            logging.error("ipv6 not supported yet")

        port = struct.unpack('!H', self.request.recv(2))[0]

        remote = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        remote.connect((address, port))
        logging.info('connected to %s : %s ' % (address, port))

        reply = struct.pack("!BBBBIH", SOCKS_VERSION, 0, 0, 1, 0, 0)
        self.request.sendall(reply)

        return remote

    def forward(self, client: socket, remote: socket):
        logging.info('exchanging data between client and remote server.')
        while True:
            r, w, e = select.select([client, remote], [], [])

            if client in r:
                data = client.recv(4096)
                if remote.send(data) <= 0:
                    break

            if remote in r:
                data = remote.recv(4096)
                if client.send(data) <= 0:
                    break


if __name__ == '__main__':
    with ThreadingTCPServer(('127.0.0.1', 9011), SocksHandler) as server:
        server.serve_forever()
